using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthScript : MonoBehaviour
{

    public Slider healthSlider;
    public Gradient gradient;
    public Image healthFill;
    public Health unitHP;

    public void SetMaxHealth(int hp){
        healthSlider.maxValue = hp;
        healthSlider.value = hp;
        unitHP = new Health(hp);

        healthFill.color = gradient.Evaluate(1f);
    }

    public void SetHealth(int hp){
        healthSlider.value = hp;
        healthFill.color = gradient.Evaluate(healthSlider.normalizedValue);
    }

    public void AddHealth(int add){
        healthSlider.value += add;
        unitHP.AddHealth(add);

        healthFill.color = gradient.Evaluate(healthSlider.normalizedValue);
    }

    public void SubHealth(int sub){
        healthSlider.value -= sub;
        unitHP.SubHealth(sub);

        healthFill.color = gradient.Evaluate(healthSlider.normalizedValue);
    }

    public bool HealthEquals(int hpref){
        return (healthSlider.value == hpref);
    }

    public bool HealthLessThan(int hpref){
        return(healthSlider.value < hpref);
    }

    public int GetHealthValue(){
        return (int)healthSlider.value;
    }
}
