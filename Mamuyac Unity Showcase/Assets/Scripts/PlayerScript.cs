using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public float speed = 6f;
    public CharacterController charCont;
    public Transform camTransform;
    public PlayerAnimationScript playerAnimScript;

    public float turnTime = 0.1f;
    float turnVelocity;

    private int atkCounter = 0;

    void Start() {
        
    }

    void Update()
    {
        MovementInput();
        AttackInput();
    }

    void MovementInput(){
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        Vector3 directionInput = new Vector3(horizontalInput, 0, verticalInput);
        directionInput.Normalize();

        if (directionInput != Vector3.zero){
            float targetAngle = Mathf.Atan2(directionInput.x, directionInput.z) * Mathf.Rad2Deg + camTransform.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnVelocity, turnTime);

            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            charCont.Move(moveDirection.normalized * speed * Time.deltaTime);
        }

        playerAnimScript.SetSpeed(Mathf.Abs(horizontalInput), Mathf.Abs(verticalInput));
    }

    void AttackInput(){
        if(Input.GetMouseButtonDown(0)){
            if(atkCounter == 0){
                atkCounter = 1;
                playerAnimScript.SetAttack("Attack1", "Attack2");
            }

            if(atkCounter == 1){
                atkCounter = 0;
                playerAnimScript.SetAttack("Attack2", "Attack1");
            }
        }
    }
}
