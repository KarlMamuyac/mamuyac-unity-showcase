using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationScript : MonoBehaviour
{
    private Animator playerAnim;

    void Start(){
        playerAnim = GetComponent<Animator>();
        playerAnim.SetTrigger("NotAttacking");
    }

    public void SetSpeed(float speedX, float speedY){
        playerAnim.SetFloat("Speed", speedX + speedY);
    }

    public void SetAttack(string mode1, string mode2){
        playerAnim.ResetTrigger(mode2);
        playerAnim.SetTrigger(mode1);
    }
}
